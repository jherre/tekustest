package com.josefernandoherreramontoya.tekustest.model.service;


public class NotificationServiceAttributes {

    public static final float noise = (float) 2.0;
    public static final String getNotificationsURL = "http://proyectos.tekus.co/Test/api/notifications/";
    public static final String deleteNotificationURL = "http://proyectos.tekus.co/Test/api/notifications/";
    public static final String authorizationHeader = "Authorization";
    public static final String authorizationHeaderValue = "Basic 900590282";

}
