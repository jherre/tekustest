package com.josefernandoherreramontoya.tekustest.model.context.notification.remote;


import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.josefernandoherreramontoya.tekustest.model.domain.NotificationTekus;
import com.josefernandoherreramontoya.tekustest.model.domain.NotificationAttributes;
import com.josefernandoherreramontoya.tekustest.model.service.NotificationServiceAttributes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class NotificationRemote {

    public static NotificationRemote getNotificationRemote(NotificationRemote notificationRemote) {

        if(notificationRemote == null) {

            notificationRemote = new NotificationRemote();

        }

        return notificationRemote;

    }

    public void getNotifications(final NotificationCompletion.NotificationErrorCompletion completion, Context context) {

        RequestQueue queue = Volley.newRequestQueue(context);

        String url = NotificationServiceAttributes.getNotificationsURL;

        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.GET, url,null,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {

                        ArrayList<NotificationTekus> notifications = new ArrayList<>();

                        for (int i = 0; i < response.length(); i++) {

                            try {

                                JSONObject row = response.getJSONObject(i);

                                String notificationId = row.getString(NotificationAttributes.notificationId);

                                Calendar date = Calendar.getInstance();

                                date.setTime(new Date(row.getString(NotificationAttributes.date).replace('-','/')));

                                int duration = row.getInt(NotificationAttributes.duration);

                                NotificationTekus notificationTekus = new NotificationTekus(notificationId, date, duration);

                                notifications.add(notificationTekus);

                            } catch (JSONException e) {

                                e.printStackTrace();

                            }

                        }

                        completion.completion(notifications, null);

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                ArrayList<NotificationTekus> notifications = new ArrayList<>();

                error.printStackTrace();

                completion.completion(notifications, error);

            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String>  params = new HashMap<String, String>();

                params.put(NotificationServiceAttributes.authorizationHeader, NotificationServiceAttributes.authorizationHeaderValue);

                return params;

            }

        };

        queue.add(stringRequest);

    }

    public void delete(String notificationId, final NotificationCompletion.NotificationErrorCompletion completion, Context context) {

        RequestQueue queue = Volley.newRequestQueue(context);

        String url = NotificationServiceAttributes.deleteNotificationURL + notificationId;

        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.DELETE, url,null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        completion.completion();

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

                completion.completion( error);

            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String>  params = new HashMap<String, String>();

                params.put(NotificationServiceAttributes.authorizationHeader, NotificationServiceAttributes.authorizationHeaderValue);

                return params;

            }

        };

        queue.add(stringRequest);

    }


    public void create(final Calendar calendar, final NotificationCompletion.NotificationErrorCompletion completion, Context context) {

        RequestQueue queue = Volley.newRequestQueue(context);

        String url = NotificationServiceAttributes.deleteNotificationURL;

        JSONObject jsonBody = new JSONObject();

        try {

            jsonBody.put(NotificationAttributes.notificationId, 0);

            String date = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss").format(calendar.getTime());

            jsonBody.put(NotificationAttributes.date, date);

            jsonBody.put(NotificationAttributes.duration, 2);


        } catch (JSONException e) {

            e.printStackTrace();

        }

        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.POST, url,jsonBody,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            NotificationTekus notificationTekus = new NotificationTekus(response.getString(NotificationAttributes.notificationId), calendar, 2);

                            completion.completion(notificationTekus, null);

                        } catch (JSONException e) {

                            e.printStackTrace();

                        }



                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

                completion.completion( error);

            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String>  params = new HashMap<String, String>();

                params.put(NotificationServiceAttributes.authorizationHeader, NotificationServiceAttributes.authorizationHeaderValue);

                return params;

            }

        };

        queue.add(stringRequest);

    }


    public void update(String notificationId, final Calendar calendar, int duration, final NotificationCompletion.NotificationErrorCompletion completion, Context context) {


        RequestQueue queue = Volley.newRequestQueue(context);

        String url = NotificationServiceAttributes.deleteNotificationURL + notificationId;

        JSONObject jsonBody = new JSONObject();

        try {

            jsonBody.put(NotificationAttributes.notificationId, 0);

            String date = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss").format(calendar.getTime());

            jsonBody.put(NotificationAttributes.date, date);

            jsonBody.put(NotificationAttributes.duration, duration);




        } catch (JSONException e) {

            e.printStackTrace();

        }

        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.PUT, url,jsonBody,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            NotificationTekus notificationTekus = new NotificationTekus(response.getString(NotificationAttributes.notificationId), calendar, 2);

                            completion.completion(notificationTekus, null);

                        } catch (JSONException e) {

                            e.printStackTrace();

                        }



                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

                completion.completion( error);

            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String>  params = new HashMap<String, String>();

                params.put(NotificationServiceAttributes.authorizationHeader, NotificationServiceAttributes.authorizationHeaderValue);

                return params;

            }

        };

        queue.add(stringRequest);

    }

}
