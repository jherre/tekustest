package com.josefernandoherreramontoya.tekustest.activities.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.android.volley.VolleyError;
import com.josefernandoherreramontoya.tekustest.R;
import com.josefernandoherreramontoya.tekustest.activities.adapters.NotificationAdapter;
import com.josefernandoherreramontoya.tekustest.activities.adapters.OnDeleteNotificationInterface;
import com.josefernandoherreramontoya.tekustest.model.context.notification.remote.NotificationCompletion;
import com.josefernandoherreramontoya.tekustest.model.context.notification.remote.NotificationRemote;
import com.josefernandoherreramontoya.tekustest.model.domain.NotificationTekus;
import java.util.ArrayList;
import java.util.List;

public class NotificationFragment extends DialogFragment implements OnDeleteNotificationInterface{

    private LinearLayoutManager layoutManager;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private Toolbar toolbar;
    private MenuItem refresh;
    private ArrayList<NotificationTekus> notifications;
    private NotificationRemote notificationRemote;
    private RelativeLayout concealer;

    @Override
    public void onAttach(Context context) {

        setStyle(DialogFragment.STYLE_NO_TITLE, 0);

        super.onAttach(context);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_notification, container, false);

        toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);

        concealer = (RelativeLayout) rootView.findViewById(R.id.concealer);

        layoutManager = new LinearLayoutManager(getContext());

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);

        recyclerView.setLayoutManager(layoutManager);

        notificationRemote = NotificationRemote.getNotificationRemote(notificationRemote);

        setupToolbar();

        setupNotifications();

        return rootView;

    }

    public void setupToolbar() {

        toolbar.inflateMenu(R.menu.refresh_menu);

        toolbar.setTitle(R.string.notifications);

        refresh = toolbar.getMenu().getItem(0);

        refresh.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {

                setupNotifications();

                return false;

            }

        });

    }

    public void setupNotifications() {

        refresh.setVisible(false);

        concealer.setVisibility(View.VISIBLE);

         notificationRemote.getNotifications(new NotificationCompletion.NotificationErrorCompletion() {

            @Override
            public void completion(List<NotificationTekus> notifications, VolleyError error) {

                NotificationFragment.this.notifications = (ArrayList<NotificationTekus>) notifications;

                adapter = new NotificationAdapter(NotificationFragment.this.notifications, getContext(), NotificationFragment.this);

                recyclerView.setAdapter(adapter);

                concealer.setVisibility(View.GONE);

                refresh.setVisible(true);

            }

            @Override
            public void completion(NotificationTekus notificationTekus, VolleyError error) {

                concealer.setVisibility(View.GONE);

                refresh.setVisible(true);

            }

             @Override
             public void completion(VolleyError error) {

             }

             @Override
             public void completion() {

             }

         }, getContext());

    }

    @Override
    public void onDeleteNotification(final NotificationTekus notificationTekus) {

        refresh.setVisible(false);

        concealer.setVisibility(View.VISIBLE);

        notificationRemote.delete(notificationTekus.getNotificationId(), new NotificationCompletion.NotificationErrorCompletion() {

            @Override
            public void completion(List<NotificationTekus> notifications, VolleyError error) {

            }

            @Override
            public void completion(NotificationTekus notificationTekus, VolleyError error) {

            }

            @Override
            public void completion(VolleyError error) {

                setupNotifications();

                concealer.setVisibility(View.GONE);

                refresh.setVisible(true);

            }

            @Override
            public void completion() {

                NotificationFragment.this.notifications.remove(notificationTekus);

                adapter = new NotificationAdapter(NotificationFragment.this.notifications, getContext(), NotificationFragment.this);

                recyclerView.setAdapter(adapter);

                concealer.setVisibility(View.GONE);

                refresh.setVisible(true);

            }

        }, getContext());

    }
}