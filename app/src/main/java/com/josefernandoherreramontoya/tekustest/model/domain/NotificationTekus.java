package com.josefernandoherreramontoya.tekustest.model.domain;

import java.util.Calendar;

public class NotificationTekus {

    private String notificationId;
    private Calendar date;
    private int duration;

    public NotificationTekus(String notificationId, Calendar date, int duration) {

        this.notificationId = notificationId;
        this.date = date;
        this.duration = duration;

    }

    public Calendar getDate() {

        return date;

    }

    public int getDuration() {

        return duration;

    }

    public String getNotificationId() {

        return notificationId;

    }

    public void setDate(Calendar date) {

        this.date = date;

    }

    public void setDuration(int duration) {

        this.duration = duration;

    }

    public void setNotificationId(String notificationId) {

        this.notificationId = notificationId;

    }

}
