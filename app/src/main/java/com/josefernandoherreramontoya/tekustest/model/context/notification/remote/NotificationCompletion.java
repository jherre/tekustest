package com.josefernandoherreramontoya.tekustest.model.context.notification.remote;

import com.android.volley.VolleyError;
import com.josefernandoherreramontoya.tekustest.model.domain.NotificationTekus;

import java.util.List;

public class NotificationCompletion {


    public interface NotificationErrorCompletion {

        void completion(List<NotificationTekus> notifications, VolleyError error);

        void completion(NotificationTekus notificationTekus, VolleyError error);

        void completion(VolleyError error);

        void completion();

    }

}
