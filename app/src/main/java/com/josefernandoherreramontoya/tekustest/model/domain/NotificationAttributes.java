package com.josefernandoherreramontoya.tekustest.model.domain;

public class NotificationAttributes {

    public static final String date = "Date";
    public static final String notificationId = "NotificationId";
    public static final String duration = "Duration";

}
