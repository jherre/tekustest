package com.josefernandoherreramontoya.tekustest.activities.adapters;

import com.josefernandoherreramontoya.tekustest.model.domain.NotificationTekus;

public interface OnDeleteNotificationInterface {

    void onDeleteNotification(NotificationTekus notificationTekus);

}
