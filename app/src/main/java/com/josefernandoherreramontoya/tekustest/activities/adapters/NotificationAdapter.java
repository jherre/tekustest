package com.josefernandoherreramontoya.tekustest.activities.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.josefernandoherreramontoya.tekustest.R;
import com.josefernandoherreramontoya.tekustest.model.domain.NotificationTekus;
import java.util.ArrayList;
import java.util.Calendar;

public class NotificationAdapter extends RecyclerView.Adapter{

    private ArrayList<NotificationTekus> notifications;
    private Context context;
    private OnDeleteNotificationInterface onDeleteNotification;

    public NotificationAdapter(ArrayList<NotificationTekus> notifications, Context context, OnDeleteNotificationInterface onDeleteNotification) {

        this.onDeleteNotification = onDeleteNotification;

        this.notifications = notifications;

        this.context = context;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification,parent, false);

        NotificationViewHolder notificationViewHolder = new NotificationViewHolder(v);

        return notificationViewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        NotificationViewHolder notificationViewHolder = (NotificationViewHolder) holder;

        notificationViewHolder.notificationId.setText(notifications.get(position).getNotificationId());

        notificationViewHolder.date.setText(dateFormat(notifications.get(position).getDate()));

        notificationViewHolder.duration.setText(String.valueOf(notifications.get(position).getDuration()));

        notificationViewHolder.delete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                onDeleteNotification.onDeleteNotification(notifications.get(position));

            }

        });

    }

    @Override
    public int getItemCount() {

        return notifications.size();

    }

    public class NotificationViewHolder extends RecyclerView.ViewHolder{

        TextView notificationId;
        TextView date;
        TextView duration;
        Button delete;

        public NotificationViewHolder(View itemView) {

            super(itemView);

            notificationId = (TextView) itemView.findViewById(R.id.notificationId);

            date = (TextView) itemView.findViewById(R.id.date);

            duration = (TextView) itemView.findViewById(R.id.duration);

            delete = (Button) itemView.findViewById(R.id.delete);

        }

    }

    private String dateFormat(Calendar calendar){

        String title = DateUtils.formatDateTime(context,

                calendar.getTimeInMillis(),

                DateUtils.FORMAT_SHOW_DATE

                        | DateUtils.FORMAT_SHOW_WEEKDAY

                        | DateUtils.FORMAT_SHOW_YEAR

                        | DateUtils.FORMAT_ABBREV_MONTH

                        | DateUtils.FORMAT_ABBREV_WEEKDAY);

        return title;

    }

}
