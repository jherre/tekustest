package com.josefernandoherreramontoya.tekustest.activities;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import com.josefernandoherreramontoya.tekustest.R;
import com.josefernandoherreramontoya.tekustest.activities.fragments.NotificationFragment;
import com.josefernandoherreramontoya.tekustest.model.service.NotificationService;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static TextView aceleText;
    private Toolbar toolbar;
    private IntentFilter filter;
    private NotificationsReceiver notificationsReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        aceleText = (TextView) findViewById(R.id.acele);

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

    }

    private boolean detectService() {

        ActivityManager am = (ActivityManager)getSystemService(Context.ACTIVITY_SERVICE);
        ArrayList<ActivityManager.RunningServiceInfo> rsiList = (ArrayList<ActivityManager.RunningServiceInfo>) am.getRunningServices (999);

        boolean found = false;

        for (ActivityManager.RunningServiceInfo rsi: rsiList) {

           if(rsi.service.getPackageName().equals(getPackageName())) {

                found = true;

                break;

            }

        }

        return found;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

       if(detectService()) {

            getMenuInflater().inflate(R.menu.service_menu_connect, menu);

        }else {

            getMenuInflater().inflate(R.menu.service_menu_start, menu);

        }


        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

       if(item.getTitle().equals(getString(R.string.action_connect)) || item.getTitle().equals(getString(R.string.action_start))) {

        Intent service = new Intent(this, NotificationService.class);

        if(! detectService()) {

            startService(service);

        }

           filter = new IntentFilter();

        filter.addAction(Intent.ACTION_INPUT_METHOD_CHANGED);

        notificationsReceiver = new NotificationsReceiver();

        registerReceiver(notificationsReceiver,filter);

           invalidateOptionsMenu();

        }else {

            showNotificationsDialog();

       }

        return true;

    }

    @Override
    protected void onDestroy() {

        if(notificationsReceiver != null) {

            unregisterReceiver(notificationsReceiver);

        }

        super.onDestroy();

    }

    public static class NotificationsReceiver extends android.content.BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            aceleText.setText(intent.getExtras().getString("eventsFromStart"));

        }

    }

    public void showNotificationsDialog() {

        NotificationFragment notificationFragment = new NotificationFragment();

        String tag = "NotificationDialog";

        notificationFragment.show(getSupportFragmentManager(), tag);

    }
}
