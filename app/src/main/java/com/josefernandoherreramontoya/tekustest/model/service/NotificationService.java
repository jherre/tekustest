package com.josefernandoherreramontoya.tekustest.model.service;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.support.annotation.Nullable;
import com.android.volley.VolleyError;
import com.josefernandoherreramontoya.tekustest.R;
import com.josefernandoherreramontoya.tekustest.model.context.notification.remote.NotificationCompletion;
import com.josefernandoherreramontoya.tekustest.model.context.notification.remote.NotificationRemote;
import com.josefernandoherreramontoya.tekustest.model.domain.NotificationTekus;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;


public class NotificationService extends Service implements SensorEventListener {

    private long startTimeStamp;
    private long lastStartTimeStamp;
    private long endTimeStamp;
    private SensorManager senSensorManager;
    private Sensor senAccelerometer;
    private String eventsFromStart;
    private float mLastX, mLastY, mLastZ;
    private NotificationRemote notificationRemote;
    private BlockingQueue tocreateQueue;
    private BlockingQueue toUpdateQueue;

    @Override
    public void onSensorChanged(SensorEvent event) {

        float x = event.values[0];
        float y = event.values[1];
        float z = event.values[2];

        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER){

            if(mLastX == 0 && mLastY == 0 && mLastZ == 0) {

                mLastX = event.values[0];

                mLastY = event.values[1];

                mLastZ = event.values[2];

            }else {

                float deltaX = Math.abs(mLastX - x);
                float deltaY = Math.abs(mLastY - y);
                float deltaZ = Math.abs(mLastZ - z);

                if (deltaX >= NotificationServiceAttributes.noise) deltaX = (float)0.0;
                if (deltaY < NotificationServiceAttributes.noise) deltaY = (float)0.0;
                if (deltaZ < NotificationServiceAttributes.noise) deltaZ = (float)0.0;

                mLastX = x;
                mLastY = y;
                mLastZ = z;

                boolean motion = deltaX != 0 || deltaY != 0 || deltaZ !=0;

                registerEvent(motion, event.timestamp);

            }

        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        senSensorManager = (SensorManager) getSystemService(this.SENSOR_SERVICE);

        senAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        senSensorManager.registerListener(this, senAccelerometer , SensorManager.SENSOR_DELAY_NORMAL);

        eventsFromStart = "initiated";

        SendRequests send = new SendRequests();

        tocreateQueue = new ArrayBlockingQueue(1024);

        toUpdateQueue = new ArrayBlockingQueue(1024);

        new Thread(send).start();
        Notification notification = new Notification.Builder(this)
                .setContentText("Servicio de alarmas")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setWhen(System.currentTimeMillis())
                .build();

        startForeground(1397, notification);

        return(START_NOT_STICKY);

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void createNotification() {

        eventsFromStart = eventsFromStart + "\n create start" + (startTimeStamp / 1000000000) + " end" + (endTimeStamp / 1000000000);

        Calendar calendar = Calendar.getInstance();

        calendar.setTimeInMillis(startTimeStamp / 1000000);

        try {

            tocreateQueue.put(new NotificationTekus("0", calendar,2));

        } catch (InterruptedException e) {

            e.printStackTrace();

        }

    }

    private void updateNotification() {

        notificationRemote = NotificationRemote.getNotificationRemote(notificationRemote);

        eventsFromStart = eventsFromStart + "\n update start"+ (startTimeStamp / 1000000000) + " end" + (endTimeStamp / 1000000000);

        Calendar calendar = Calendar.getInstance();

        calendar.setTimeInMillis(startTimeStamp / 1000000);

        int duration = (int) ((endTimeStamp / 1000000000) - (startTimeStamp / 1000000000));

        try {

            toUpdateQueue.put(new NotificationTekus("-1", calendar, duration));

        } catch (InterruptedException e) {

            e.printStackTrace();

        }

        Intent intent = new Intent();

        intent.setAction(Intent.ACTION_INPUT_METHOD_CHANGED);

        intent.putExtra("eventsFromStart", eventsFromStart);

        sendBroadcast(intent);

    }

    private void registerEvent(boolean motion, long timeStamp) {

        if (motion) {

            if(startTimeStamp == 0) {

                startTimeStamp = timeStamp;

            }else {

                endTimeStamp = timeStamp;

                if(lastStartTimeStamp != startTimeStamp) {

                    if(endTimeStamp - startTimeStamp > 2000000000) {

                        createNotification();

                        lastStartTimeStamp = startTimeStamp;

                    }

                }

            }

        }else {

            if(endTimeStamp - startTimeStamp > 2000000000) {

                updateNotification();

            }

            startTimeStamp = 0;

            endTimeStamp = 0;

        }

    }

    public class SendRequests implements Runnable {

        final String toCreate = "create";
        final String toUpdate = "update";
        NotificationTekus created;
        String state;

        @Override
        public void run() {

            state = toCreate;

            while(true) {

                if(state.equals(toCreate)) {

                    try {

                        NotificationTekus n = (NotificationTekus) tocreateQueue.take();


                    create(n);

                    } catch (InterruptedException e) {

                        e.printStackTrace();

                    }

                }else {

                    try {

                        NotificationTekus n = (NotificationTekus) toUpdateQueue.take();

                        update(n);

                    } catch (InterruptedException e) {

                        e.printStackTrace();

                    }

                }

            }

        }


        private void update(final NotificationTekus notificationTekusBefore) {

            notificationRemote = NotificationRemote.getNotificationRemote(notificationRemote);

            if(created != null) {

                notificationRemote.update(created.getNotificationId(), notificationTekusBefore.getDate(), notificationTekusBefore.getDuration(), new NotificationCompletion.NotificationErrorCompletion() {
                    @Override
                    public void completion(List<NotificationTekus> notifications, VolleyError error) {


                    }

                    @Override
                    public void completion(NotificationTekus notificationTekus, VolleyError error) {

                        created = null;
                        state = toCreate;

                    }

                    @Override
                    public void completion(VolleyError error) {

                    }

                    @Override
                    public void completion() {

                    }

                }, NotificationService.this);

            }

        }


        private void create(final NotificationTekus notificationTekusBefore) {

            notificationRemote = NotificationRemote.getNotificationRemote(notificationRemote);

            notificationRemote.create(notificationTekusBefore.getDate(), new NotificationCompletion.NotificationErrorCompletion() {
                @Override
                public void completion(List<NotificationTekus> notifications, VolleyError error) {

                }

                @Override
                public void completion(NotificationTekus notificationTekus, VolleyError error) {

                    created = notificationTekus;

                    state = toUpdate;

                }

                @Override
                public void completion(VolleyError error) {

                }

                @Override
                public void completion() {

                }

            },  NotificationService.this);
        }

    }

}
